﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class TowerManager : NetworkBehaviour {

    public static TowerManager singleton;

    void Awake() {
        if (singleton != null) {
            Debug.LogError("More than one Tower manager is running");
        } else {
            singleton = this;
        }
    }
    
    #region tower tracking 
    private const string TOWER_ID_PREFIX = "Tower ";
    public static Dictionary<string, Tower> towers = new Dictionary<string, Tower>();
    
    public static Tower GetTower(string _towerID) {
        if (towers.ContainsKey(_towerID))
            return towers[_towerID];
        print("Couldn't find tower with ID: " + _towerID);
        return default(Tower);
    }
    //Find the closest tower to position
    public static Tower GetClosestTower(Vector3 position) {
        Tower currentClosest = default(Tower);
        float currentClosestDistance = 0f;
        foreach (Tower tower in towers.Values) {
            float distance = Vector3.Distance(tower.gameObject.transform.position, position);
            if (distance < currentClosestDistance || currentClosestDistance == 0f) {
                currentClosest = tower;
                currentClosestDistance = distance;
            }
        }
        if (currentClosest != default(Tower)) {
            return currentClosest;
        }
        Debug.LogError("No tower found. Returned default tower likely breaking script using this method");
        return default(Tower);
    }
    #endregion
}
