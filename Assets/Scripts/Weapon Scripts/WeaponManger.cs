﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class WeaponManger : NetworkBehaviour {

	[SerializeField]
	private Transform GunHolder; 

	[SerializeField]
	private BaseWeapon primaryWeapon; 

	private BaseWeapon currentWeapon; // cant set sync var

	[SerializeField]
	private List<int> weaponIDs;

	[SerializeField]
	private List<BaseWeapon> baseWeapons;

	[SerializeField]
	private List<GameObject> weapons;
	private int weaponIndex;

	[SerializeField]
	private string weaponLayerName = "Weapon"; // Other guns could be hidden in other layers

	public bool isActive = false;


	void Start () {
		//at the moment, this means that we start with a weapon equipteded 

		//baseWeapons = new List<BaseWeapon>(); //Set all possible prefabs loadable  This should hold everything then just activate them from list. 

		weapons = new List<GameObject>(); //empty? or set to 0? 
		//EquipWeapon (primaryWeapon);

		weaponIndex = 0;

		Debug.Log("weapon start start complete for player"); 
		//Debug.Log("aseWeapons.Count " + baseWeapons.Count ); 




		//THE M9 IS TO BE PHASED OUT
		currentWeapon = primaryWeapon;//baseWeapons [weaponIndex];

		Debug.Log(currentWeapon); 

		EquipWeapon(currentWeapon);

		Debug.Log(currentWeapon); 

	}

	void Update(){

		if (!isLocalPlayer) {
			//Renderer rend = GetComponent<Renderer>();
			//rend.enabled = false;
			currentWeapon.GetComponentInChildren<Renderer>().enabled = false;
		}

		if (GameManager.singleton.state == GameManager.GameState.LOST) {
			MakeCurrentWeaponUnEnabled ();
		} else {

			isActive = (currentWeapon != null) && currentWeapon.gunID != 0;

			if (isActive) {
				if (Input.GetAxis ("Mouse ScrollWheel") > 0f) {
					CycleWeaponDown ();
				} else if (Input.GetAxis ("Mouse ScrollWheel") < 0f) {
					CycleWeaponUp ();
				}

				MakeCurrentWeaponEnabled ();
			} 
		}


	}


	/// <summary>
	/// This might need to be an RPC call, I was thinking of making the current weapon id a sync var
	/// then check each turn if it is the current one, if not then force change. not sure yet. Need to make
	/// sure this function is networked properly 
	/// </summary>
	/// <param name="_weapon">Weapon.</param>
	public void EquipWeapon(BaseWeapon _weapon)
	{
		if (!weaponIDs.Contains (_weapon.gunID)) { //this just stop players from picking up guns they have 
			weaponIDs.Add (_weapon.gunID);

			MakeCurrentWeaponUnEnabled ();

			Debug.Log (_weapon); 

			baseWeapons.Add (_weapon); 

			GameObject WeaponInstance = (GameObject)Instantiate (_weapon.graphics, GunHolder.position, GunHolder.rotation);
			weapons.Add (WeaponInstance);

			if (currentWeapon != null) {
				ChangeToWeapon (_weapon);
			}
			currentWeapon = _weapon;


			WeaponInstance.transform.SetParent (GunHolder); //this is so it will follow it and move around 

		}
	}

	public bool addAmmo (int ammount)
	{
		return (weapons.ToArray () [baseWeapons.IndexOf (currentWeapon)]).GetComponent<BaseWeapon> ().addAmmo (ammount);//return currentWeapon.addAmmo (ammount);
	}

	//A getter for the wepaon
	public BaseWeapon GetCurrentWeapon()
	{
		return currentWeapon;
	}

	public void CycleWeaponUp(){
		if (weaponIndex < weapons.Count - 1) {
			MakeCurrentWeaponUnEnabled ();
			weaponIndex += 1;
			BaseWeapon weapon = baseWeapons.ToArray () [weaponIndex];
			ChangeToWeapon (weapon);
		}
	}

	public void CycleWeaponDown(){
		if (weaponIndex > 0) {
			MakeCurrentWeaponUnEnabled ();
			weaponIndex -= 1;
			BaseWeapon weapon = baseWeapons.ToArray () [weaponIndex];
			ChangeToWeapon (weapon);
		}
	}

	public void ChangeToWeapon(BaseWeapon _weapon){
		currentWeapon = _weapon;
		if (!(weapons.ToArray () [baseWeapons.IndexOf (currentWeapon)].activeSelf)) 
		{
			weapons.ToArray () [baseWeapons.IndexOf (currentWeapon)].SetActive (true);
		}


	}


	void MakeCurrentWeaponUnEnabled(){ 
		
		if (weapons.Count > 0) 
		{
			weapons.ToArray () [baseWeapons.IndexOf (currentWeapon)].SetActive (false);
		}
	}

	void MakeCurrentWeaponEnabled(){ 

		if (weapons.Count > 0) 
		{
			weapons.ToArray () [baseWeapons.IndexOf (currentWeapon)].SetActive (true);
		}
	}

	public bool ContainsWeapon(BaseWeapon weapon){
		for (int i = 0; i < weapons.Count; i++) {
			if (weapons.ToArray () [i].GetComponent<BaseWeapon> ().gunID == weapon.gunID) {
				return true;
			}
		}
		return false;
	}
	

}
