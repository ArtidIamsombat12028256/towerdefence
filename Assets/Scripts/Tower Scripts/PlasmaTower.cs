﻿using UnityEngine;
using System.Collections;

public class PlasmaTower : Tower {

	// Use this for initialization
	void Start () {

	}

	public PlasmaTower(string type, GameObject prefab, int price, Vector3 pos) :base(type, prefab, price, pos){

	}
	public PlasmaTower(GameObject prefab, int price) :base(prefab, price){
	}

	public PlasmaTower(Tower tower) :base(tower){

	}
	public PlasmaTower (GameObject prefab) :base(prefab){

	}
	
	// Update is called once per frame
	void Update () {
		if (this.transform.position.y < 0) {
			this.transform.position += new Vector3 (0, 0.05f, 0);//1f, 0);
		}
	}
}
