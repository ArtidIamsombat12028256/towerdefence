﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Collections.Generic;

public class Enemy : BaseHealth {
    [SerializeField]
    private List<Transform> edges;
    public const string PLACEHOLDER = "Placeholder";
	public const string FLYING = "Flying";
	public const string FASTFLYING = "FastFlying";
    [SerializeField]
    public string type;
	[SerializeField]//offsetToUpdatePath is how far the current target is away from the end of the current path in order to get a new path
	protected float weaponDamage, weaponRange, offsetToUpdatePath, moveSpeed, rotSpeed, distanceToIgnorePath; 
	protected int currentPathIndex = 0;
	protected int pathAheadCheck = 5;
	protected int enemyId;

	protected List<Vector3> path;
	protected Vector3 moveTarget;
	protected Vector3 targetOldPos;

	protected Pathfinder pathfinder;
	protected Transform playerBase;
    protected Vector3 targetPos;

    public GameObject explosion;
	public AudioSource hummingSource;
	public AudioClip hummingSound;
	public AudioClip hummingDamageSound;
	public AudioSource damageSource;
	public AudioClip[] ricochetSounds;
	public AudioClip explosionSound;
    [SyncVar]
    Vector3 pos;
    [SyncVar]
    Vector3 forwards;
    [SyncVar]
    Vector3 upwards;
    public ParticleSystem sparks;
	public ParticleSystem damageSparks;

	public int fundsWorth;

	public bool isDead = false;
    Rigidbody rb;
	public enum Target { PlayerBase, Player, Turret }
	private Target _target = Target.PlayerBase;
	public Target target {
		get { return _target; }
		set { _target = value; }
	}
    private Target oldTarget;
    private float distanceToTarget;
    private Vector3 oldTargetPos;
    Vector3 currentNodePos;
    float distanceToPathNode;
    float pathLastUpdated;

    int prevIndex = 0;
    public virtual void Start() {
		pathfinder = new Pathfinder();
		playerBase = GameObject.Find ("EndZone").transform;
		//UpdatePath ();
        rb = GetComponent<Rigidbody>();
        //  if(GameObject.FindGameObjectWithTag("InWall"))
        //Physics.IgnoreCollision (GetComponent<BoxCollider> (), GameObject.FindGameObjectWithTag ("InWall").GetComponent<BoxCollider>());
        rb.freezeRotation = true;

        hummingSource.clip = hummingSound;
		hummingSource.Play ();
	}
    bool CheckLineOfSight() {
        RaycastHit hit;
        List<RaycastHit> hits = new List<RaycastHit>();
        foreach (Transform edge in edges) {
            if (Physics.Raycast(edge.position, targetPos, out hit, distanceToTarget)) {
                if(hit.transform.tag == "Map") {
                    return false;
                }
                hits.Add(hit);
            }
        }
        foreach(RaycastHit h in hits) {
            if (target == Target.Player && h.transform.root.gameObject.GetComponent<Player>()) {
                return true;
            } else if (target == Target.PlayerBase && h.transform.root.gameObject.GetComponent<Base>()) {
                return true;
            }
        }
        return false;
    }
    protected virtual void Update() {
        if (!isServer) {
            // transform.position = Vector3.Lerp(transform.position, pos, 1.1f*Time.deltaTime);
        }
        pathLastUpdated += Time.deltaTime;
        if (currentHealth <= maxHealth / 2) {
            if (hummingSource.clip.GetInstanceID() != hummingDamageSound.GetInstanceID()) {
                hummingSource.Stop();
                hummingSource.clip = hummingDamageSound;
                hummingSource.Play();
            }

            if (!damageSparks.isPlaying) {
                damageSparks.Play();
            }
        }
        
    }
    bool GetPath() {
        path = pathfinder.FindVector3Path(transform.position, targetPos, false);
        if (path == new List<Vector3>())
            return false;
        EnemyManager.pathsUpdated++;
        pathLastUpdated = 0;
        currentPathIndex = 0;
        return true;
    }
    void OnDrawGizmos() {
        if (path == null) return;
        Vector3 prevPos = Vector3.zero;
        foreach(Vector3 pathNode in path) {
            if (prevPos == Vector3.zero) prevPos = transform.position;
            Vector3 nodePos = pathNode;
            Gizmos.DrawLine(nodePos, prevPos);
            prevPos = nodePos;
        }
    }
    
    protected virtual void FixedUpdate() {
        if (!isServer) {
            transform.position = Vector3.Lerp(transform.position, pos, moveSpeed * 1.1f);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(forwards, upwards), rotSpeed * 1.1f);
            return;
        }
        if (CheckLineOfSight()) {
            print("Dog");
            Move(Rotate(targetPos));
            return;
        }
        pos = transform.position;
        forwards = transform.forward;
        upwards = transform.up;
        if (path != null && path.Count > 0) {
            currentNodePos = path[currentPathIndex];
        }
        if (path == null) {
            UpdateTarget();
            GetPath();
        }else if (distanceToTarget < distanceToIgnorePath) {
            Move(Rotate(targetPos));
        } else {
            UpdateTarget();
            if (path.Count > currentPathIndex) {
                moveTarget = path[currentPathIndex];
            }
            Move(Rotate(moveTarget));
        }
        prevIndex = currentPathIndex;
    }
    void Move(Vector3 _dir) {
        transform.position += transform.forward * moveSpeed;
        if(distanceToPathNode < offsetToUpdatePath) {
            if (currentPathIndex < path.Count-2) {
                currentPathIndex++;
            }
        }
    }
    Vector3 Rotate(Vector3 _targetPos) {
        _targetPos = new Vector3(_targetPos.x, transform.position.y, _targetPos.z);
        Vector3 dir = _targetPos- transform.position;
        dir.Normalize();
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir), rotSpeed);
        return dir;
    }
    private void UpdateTarget() {
        Vector3 closestPlayer = GameManager.GetClosestPlayer(transform.position).transform.position;
        float distanceToPlayer = Vector3.Distance(closestPlayer, transform.position);
        float distanceToBase = Vector3.Distance(playerBase.position, transform.position);
        target = distanceToBase > distanceToPlayer ? Target.Player : Target.PlayerBase;
        targetPos = target == Target.Player ? closestPlayer : playerBase.position;
        distanceToTarget = Vector3.Distance(targetPos, transform.position);
        if (path == null) return;
        distanceToPathNode = Vector3.Distance(transform.position, currentNodePos);
        float pathDivergence = 0;
        if(path.Count >0)
            pathDivergence = target == Target.Player ? Vector3.Distance(targetPos, path[path.Count - 1]) : 0;
        //print("Pathdivergence: " + pathDivergence + ", PathsUpdated: " + EnemyManager.pathsUpdated + ", pathLastUpdated: " + pathLastUpdated);
        if ((target != oldTarget || 
            (pathDivergence > .5f && path.Count - currentPathIndex < 5) ||
            pathDivergence > 3 ) &&
            EnemyManager.pathsUpdated < 2 &&
            pathLastUpdated > .25f) {
            
            GetPath();
        }
        oldTarget = target;
        oldTargetPos = targetPos;
    }
    void OnTriggerEnter(Collider collider) {
        if (collider.transform.name == "EndZone") {
            Die();
            return;
        }
        //UpdatePath();
    }
    void OnCollisionEnter(Collision collision) {
        foreach (ContactPoint contact in collision.contacts)
            if (contact.otherCollider.transform.name == "EndZone") {
                Die();
                return;
            } else if (contact.otherCollider.transform.name.Contains("Player")) {
                Die();
                return;
            }
       // UpdatePath();
    }
    protected override void Die() {
        if (isServer) {
            Explode();

            Enemy enemy = default(Enemy);
            string ID = "";
            foreach (KeyValuePair<string, Enemy> kp in EnemyManager.enemies) {
                if (kp.Value.transform.position == transform.position) {
                    enemy = kp.Value;
                    ID = kp.Key;
                    break;
                }
            }
            if (enemy != default(Enemy)) {

                EnemyManager.enemies.Remove(ID);
                NetworkServer.Destroy(enemy.gameObject);
            }
        }
    }
    protected virtual void Explode() {
        //damage playerbase if in range
        if (Vector3.Distance(playerBase.position, transform.position) < weaponRange) {
            //	if (!GetComponentInChildren<ParticleSystem> ().isPlaying) {
            playerBase.GetComponent<Base>().TakeDamage(weaponDamage);
            Debug.Log("Enemy hit base for: " + weaponDamage);
            // }
        }
        //damage players in range
        List<Player> playersInRange = GameManager.GetPlayersInRange(transform.position, weaponRange);
        if (playersInRange.Count > 0) {
            foreach (Player player in playersInRange) {
                player.TakeDamage(weaponDamage);

            }
        }
        // RpcExplode(transform.position);
        GameObject exp = (GameObject)GameObject.Instantiate(explosion, transform.position, new Quaternion());
        NetworkServer.Spawn(exp);
        isDead = true;
    }

    public override void TakeDamage(float _dmg) {
     
        sparks.Play();

        //For playing sounds only.
        int i = UnityEngine.Random.Range(0, ricochetSounds.Length);

        AudioClip clip = ricochetSounds[i];
        damageSource.Stop();
        damageSource.clip = clip;
        damageSource.Play();
        base.TakeDamage(_dmg);
    }
   
}
